#
# Makefile
#


APP_NAME := test22

${APP_NAME}: test.c
	@echo [CC] $@
	@${CC} ${CCFLAGS} test.c -o $@ ${LDFLAGS}

clean:
	@rm -f ${APP_NAME}

install:
	@mkdir -p ${DESTDIR}/${BINDIR}
	@install -m 0755 ${APP_NAME} ${DESTDIR}/${BINDIR}

.PHONY: clean install
