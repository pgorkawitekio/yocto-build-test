/**
 * @file
 * @author Pawel Gorka <pgorka@witekio.com>
 * @brief Simple C application.
 */
 
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
  printf("Appending to file\n");
FILE *fp = fopen("/home/test.txt", "ab+");
if(fp) {
   fprintf(fp,"TEST\n");
    fclose(fp);
} else {
    printf("There was a problem creating a file\n");
    exit(1);

}

  return 0;
}
